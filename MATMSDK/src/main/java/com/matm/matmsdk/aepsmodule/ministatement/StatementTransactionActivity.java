package com.matm.matmsdk.aepsmodule.ministatement;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.matm.matmsdk.aepsmodule.utils.AepsSdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

import isumatm.androidsdk.equitas.R;

public class StatementTransactionActivity extends AppCompatActivity {
    LinearLayout failureLayout;
    RelativeLayout successLayout;
    Button okButton,okSuccessButton;
    TextView transaction_details_header_txt,failureDetailTextView,failureTitleTextView,aadhar_num_txt,account_balance_txt,transaction_id_txt,bank_name_txt;
    String aadharCardStr ="";
    String amount ="";
    Session session;
    ProgressDialog pd;
    RecyclerView statement_list;
    StatementList_Adapter statementList_adapter;
    public Pattern DATE_PATTERN1 = Pattern.compile(
            "^\\d{2}/\\d{2}/\\d{4}$");
    public Pattern DATE_PATTERN2 = Pattern.compile(
            "^\\d{2}/\\d{2}$");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statement_transaction);
        session = new Session(StatementTransactionActivity.this);
        pd = new ProgressDialog(StatementTransactionActivity.this);
        statement_list = findViewById(R.id.statement_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(StatementTransactionActivity.this);
        statement_list.setLayoutManager(linearLayoutManager);
        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        transaction_details_header_txt = findViewById(R.id.transaction_details_header_txt);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);
        aadhar_num_txt = findViewById(R.id.aadhar_num_txt);
        account_balance_txt = findViewById(R.id.account_balance_txt);
        transaction_id_txt = findViewById(R.id.transaction_id_txt);
        bank_name_txt = findViewById(R.id.bank_name_txt);
        String repsonse = getIntent().getStringExtra(AepsSdkConstants.TRANSACTION_STATUS_KEY);



        if(repsonse.equalsIgnoreCase("500")){
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView .setText ("Internal server error.");
        }else{
            Gson gson = new Gson();
            StatementResponse statementResponse = gson.fromJson(repsonse,StatementResponse.class);

            failureLayout.setVisibility ( View.GONE );
            successLayout.setVisibility ( View.VISIBLE );
            aadhar_num_txt.setText(statementResponse.getCustomerAadhaarNo());
            transaction_id_txt.setText(statementResponse.getTxId());
            bank_name_txt.setText(statementResponse.getBankName());

            if(statementResponse.getMiniStatementString()!=null) {
                if (statementResponse.getStatus().equalsIgnoreCase("-1")) {
                    failureLayout.setVisibility ( View.VISIBLE );
                    successLayout.setVisibility ( View.GONE );
                    failureDetailTextView .setText (statementResponse.getApiComment());
                }
                else {
                    try {
                        JSONObject jsonObject = getJSON(statementResponse.getMiniStatementString());
                        String balance = jsonObject.getString("total_balance");
                        JSONArray jsonArray = jsonObject.getJSONArray("transactions");
                        account_balance_txt.setText(balance);
                        if (jsonArray.length() != 0) {
                            statementList_adapter = new StatementList_Adapter(this, jsonArray);
                            statement_list.setAdapter(statementList_adapter);
                        } else {
                            transaction_details_header_txt.setText("No Transaction Details Found");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
            }else{
                transaction_details_header_txt.setText("No Transaction Details Found");
            }

        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent respIntent = new Intent();
                setResult(Activity.RESULT_OK,respIntent);
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent respIntent = new Intent();
                setResult(Activity.RESULT_OK,respIntent);
                finish();

            }
        });

    }


    public JSONObject getJSON(String response1){
        JSONObject mainObject = new JSONObject();
        try{
            ArrayList<String> transactionSlots = new ArrayList<>();
            String a = response1.substring(28,31);
            int totalTransactionRecord = Integer.valueOf((response1.substring(28,31)))/35;
            String transactionRecord = response1.substring(31,response1.length());
            String transactionBalance = transactionRecord;
            for (int i=0;i<totalTransactionRecord-1;i++){
                String tempResponse = transactionBalance;
                tempResponse = tempResponse.substring(0,35);
                transactionSlots.add(tempResponse);
                if(transactionBalance.length()>=35) {
                    transactionBalance = transactionBalance.substring(35);
                }
            }

            if(transactionBalance.contains("Balance")){
                transactionBalance = transactionBalance.replaceAll("Balance","").trim();
            }else if(transactionBalance.contains("AVAIL BAL")){
                transactionBalance = transactionBalance.replaceAll("AVAIL BAL","").trim();
            }


            JSONArray jsonArray = new JSONArray();

            for(int j=0;j<transactionSlots.size();j++){
                JSONObject jsonObject = new JSONObject() ;
                String[] tempSlots = transactionSlots.get(j).split(" ");
                ArrayList<String> tempRemark = new ArrayList<String>(Arrays.asList(tempSlots));
                for(int k=0;k<tempSlots.length;k++){
                    if(tempSlots[k].trim().equalsIgnoreCase("D") || tempSlots[k].trim().equalsIgnoreCase("Dr")){
                        jsonObject.put("transaction_type","Debit");
                        tempRemark.set(k,"");
                    }
                    else if(tempSlots[k].trim().equalsIgnoreCase("C") || tempSlots[k].trim().equalsIgnoreCase("Cr")){
                        jsonObject.put("transaction_type","Credit");
                        tempRemark.set(k,"");
                    }
                    else if(tempSlots[k].trim().contains(".")){
                        jsonObject.put("transaction_amount",tempSlots[k].trim());
                        tempRemark.set(k,"");
                    }
                    else if(DATE_PATTERN1.matcher(tempSlots[k].trim()).matches() || DATE_PATTERN2.matcher(tempSlots[k].trim()).matches()){
                        jsonObject.put("transaction_date",tempSlots[k].trim());
                        tempRemark.set(k,"");
                    }
                }

                jsonObject.put("transaction_remark",getRemarks(tempRemark));
                jsonArray.put(jsonObject);
            }

            mainObject.put("transactions",jsonArray);
            mainObject.put("total_balance",transactionBalance.trim());

            System.out.println("JSON"+mainObject);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mainObject;
    }

    public String getRemarks(ArrayList<String> tempRemark){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<tempRemark.size();i++){
            if(tempRemark.get(i).toString().length()!=0){
                sb.append(" "+tempRemark.get(i));
            }
        }
        return sb.toString().trim();
    }

}
