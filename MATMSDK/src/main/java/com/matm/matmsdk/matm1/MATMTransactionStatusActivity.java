package com.matm.matmsdk.matm1;


import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import isumatm.androidsdk.equitas.R;


public class MATMTransactionStatusActivity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matmtransaction_status);

        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(MatmConstant.MICRO_ATM_TRANSACTION_STATUS_KEY) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occurred \n Please try again after some time !!!");
        }else{
            MicroAtmTransactionModel microAtmTransactionModel = (MicroAtmTransactionModel) getIntent().getSerializableExtra(MatmConstant.MICRO_ATM_TRANSACTION_STATUS_KEY);

            if (microAtmTransactionModel .getTxnStatus() !=null && microAtmTransactionModel.getTxnAmt() !=null ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                String txnstatus = "NA";
                if(microAtmTransactionModel.getTxnStatus() !=null && !microAtmTransactionModel.getTxnStatus().matches("")){
                    txnstatus = microAtmTransactionModel.getTxnStatus();
                }

                String balance = "NA";
                if(microAtmTransactionModel.getAvailableBalance() !=null && !microAtmTransactionModel.getAvailableBalance().matches("NA")){
                    balance = microAtmTransactionModel.getAvailableBalance();
                }

                String cardnumber = "NA";
                if(microAtmTransactionModel.getCardNumber() != null && !microAtmTransactionModel.getCardNumber().matches("")){
                    cardnumber = microAtmTransactionModel.getCardNumber();
                }
                String rferencesnumber = "NA";
                if(microAtmTransactionModel.getRnr()!= null && !microAtmTransactionModel.getRnr().matches("")){
                    rferencesnumber = microAtmTransactionModel.getRnr();
                }

                String dataandtime = "NA";
                if(microAtmTransactionModel.getTransactionDateandTime() !=null && !microAtmTransactionModel.getTransactionDateandTime().matches("")){
                    dataandtime = microAtmTransactionModel.getTransactionDateandTime();
                }

                String amount = "NA";
                if(microAtmTransactionModel.getTxnAmt() !=null && !microAtmTransactionModel.getTxnAmt().matches("")){
                    amount = microAtmTransactionModel.getTxnAmt();
                }

                String terminal = "NA";
                if(microAtmTransactionModel.getTerminalId() !=null && !microAtmTransactionModel.getTerminalId().matches("")){
                    terminal = microAtmTransactionModel.getTerminalId();
                }

                if(microAtmTransactionModel.getType() !=null && !microAtmTransactionModel.getType().matches("")){
                    detailsTextView.setText ( "Cash Withdrawal for " +cardnumber + " is sucessfully completed . \n \nTransaction Status : "+txnstatus+" \nReference No : "+ rferencesnumber+"\nAvailable Balance : "+ balance+"\nTransaction Amount : "+amount+ " \nTransaction Date and Time : "+dataandtime+" \nTerminal ID : "+terminal);
                }

            }else  if (microAtmTransactionModel.getBalanceEnquiryStatus() != null && microAtmTransactionModel.getAccountNo() != null){
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);

                String balanceEnquiryStatus = "NA";
                if(microAtmTransactionModel.getBalanceEnquiryStatus() !=null && !microAtmTransactionModel.getBalanceEnquiryStatus().matches("")){
                    balanceEnquiryStatus = microAtmTransactionModel.getBalanceEnquiryStatus();
                }

                String balance = "NA";
                if(microAtmTransactionModel.getAvailableBalance() !=null && !microAtmTransactionModel.getAvailableBalance().matches("NA")){
                    balance = microAtmTransactionModel.getAvailableBalance();
                }

                String cardnumber = "NA";
                if(microAtmTransactionModel.getCardNumber() != null && !microAtmTransactionModel.getCardNumber().matches("")){
                    cardnumber = microAtmTransactionModel.getCardNumber();
                }
                String rferencesnumber = "NA";
                if(microAtmTransactionModel.getRnr()!= null && !microAtmTransactionModel.getRnr().matches("")){
                    rferencesnumber = microAtmTransactionModel.getRnr();
                }

                String dataandtime = "NA";
                if(microAtmTransactionModel.getTransactionDateandTime() !=null && !microAtmTransactionModel.getTransactionDateandTime().matches("")){
                    dataandtime = microAtmTransactionModel.getTransactionDateandTime();
                }

                String accountNumber = "NA";
                if(microAtmTransactionModel.getAccountNo() !=null && !microAtmTransactionModel.getAccountNo().matches("")){
                    accountNumber = microAtmTransactionModel.getAccountNo();
                }

                String terminal = "NA";
                if(microAtmTransactionModel.getTerminalId() !=null && !microAtmTransactionModel.getTerminalId().matches("")){
                    terminal = microAtmTransactionModel.getTerminalId();
                }

                if(microAtmTransactionModel.getType() !=null && !microAtmTransactionModel.getType().matches("")){
                    detailsTextView.setText (balanceEnquiryStatus+" for " +cardnumber + ". \n \nAccount No : "+accountNumber+" \nReference No : "+ rferencesnumber+"\nAvailable Balance : "+ balance+" \nTransaction Date and Time : "+dataandtime+" \nTerminal ID : "+terminal);
                }
            }else if(microAtmTransactionModel.getStatusError() != null && microAtmTransactionModel.getErrormsg() != null &&
                    !microAtmTransactionModel.getStatusError().matches("") && !microAtmTransactionModel.getErrormsg().matches("") &&
                    microAtmTransactionModel.getStatusError().trim().equalsIgnoreCase("success")) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    detailsTextView.setText(microAtmTransactionModel.getErrormsg());
                    detailsTextView.setGravity(Gravity.CENTER);
                }
            }else if(microAtmTransactionModel.getStatusError() != null && microAtmTransactionModel.getErrormsg() != null &&
                    !microAtmTransactionModel.getStatusError().matches("") && !microAtmTransactionModel.getErrormsg().matches("") &&
                    microAtmTransactionModel.getStatusError().trim().equalsIgnoreCase("failed")) {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    failureDetailTextView.setText(microAtmTransactionModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again !!!");
                }
            }else{
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    failureDetailTextView .setText (microAtmTransactionModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again after some time !!!");
                }
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
