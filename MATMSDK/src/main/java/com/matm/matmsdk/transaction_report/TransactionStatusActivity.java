package com.matm.matmsdk.transaction_report;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.matm.matmsdk.ChooseCard.ChooseCardActivity;
import com.matm.matmsdk.Dashboard.DashBoardActivity;
import com.matm.matmsdk.Dashboard.MainActivity;
import isumatm.androidsdk.equitas.R;
import com.matm.matmsdk.Service.BankResponse;
import com.matm.matmsdk.Utils.MATMSDKConstant;
import com.matm.matmsdk.Utils.PAXScreen;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TransactionStatusActivity extends AppCompatActivity {
    private LinearLayout ll_maiin;
    private TextView statusMsgTxt,statusDescTxt;
    private ImageView status_icon;
    private TextView appl_name,a_id,date_time,rref_num,mid,tid,txnid,invoice,card_id,appr_code,card_no,card_amount,card_transaction_amount,txn_id;
    public EasyLinkSdkManager manager;
    private Button backBtn;
    ChooseCardActivity chooseCardActivity;
    LinearLayout ll13,ll12;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        ll_maiin = findViewById(R.id.ll_maiin);
        manager = EasyLinkSdkManager.getInstance(this);

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }*/

        ll13 = findViewById(R.id.ll13);
        ll12 = findViewById(R.id.ll12);



        card_transaction_amount = findViewById(R.id.card_transaction_amount);
        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        appl_name = findViewById(R.id.appl_name);
        a_id = findViewById(R.id.a_id);
        rref_num = findViewById(R.id.rref_num);
        mid = findViewById(R.id.mid);
        tid = findViewById(R.id.tid);
        txnid = findViewById(R.id.txnid);
        invoice = findViewById(R.id.invoice);
        card_id = findViewById(R.id.card_id);
        appr_code = findViewById(R.id.appr_code);
        card_no = findViewById(R.id.card_no);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        date_time = findViewById(R.id.date_time);
        txn_id = findViewById(R.id.txn_id);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);
        String transaction_type = getIntent().getStringExtra("TRANSACTION_TYPE");
        String transaction_amount = getIntent().getStringExtra("TRANSACTION_AMOUNT");
        String TRANSACTION_ID = getIntent().getStringExtra("TRANSACTION_ID");


        String applName = getIntent().getStringExtra("APP_NAME");
        String aId = getIntent().getStringExtra("AID");
        String prefNum = getIntent().getStringExtra("RRN_NO");
        String MID = getIntent().getStringExtra("MID");
        String TID = getIntent().getStringExtra("TID");
        String TXN_ID = getIntent().getStringExtra("TXN_ID");
        String INVOICE = getIntent().getStringExtra("INVOICE");

        String CARD_TYPE = getIntent().getStringExtra("CARD_TYPE");
        String APPR_CODE = getIntent().getStringExtra("APPR_CODE");
        String CARD_NUMBER = getIntent().getStringExtra("CARD_NUMBER");
        String AMOUNT = getIntent().getStringExtra("AMOUNT");
        String RESPONSE_CODE = getIntent().getStringExtra("RESPONSE_CODE");
        RESPONSE_CODE = RESPONSE_CODE.substring(2);

        if(AMOUNT.equalsIgnoreCase("0")){
            AMOUNT = "0.00";
        }else{
            AMOUNT = replaceWithZero(AMOUNT);
        }

        System.out.println(">>>----"+AMOUNT);

        String[] splitAmount = CARD_NUMBER.split("D");
        CARD_NUMBER = splitAmount[0];

        String firstnum = CARD_NUMBER.substring(0,2);
        String middlenum = CARD_NUMBER.substring(2,CARD_NUMBER.length()-2);
        String lastNum = CARD_NUMBER.replace(firstnum+middlenum,"");

        System.out.println(">>>---"+firstnum);
        System.out.println(">>>---"+middlenum);
        System.out.println(">>>---"+lastNum);

        if(transaction_type.equalsIgnoreCase("cash")){
            ll13.setVisibility(View.VISIBLE);
            card_amount.setText(AMOUNT);
            card_transaction_amount.setText(replaceWithZero(transaction_amount));
        }else{
            ll13.setVisibility(View.GONE);
            ll12.setVisibility(View.VISIBLE);
            card_amount.setText(AMOUNT);
        }


       // CARD_NUMBER =
        String flag = getIntent().getStringExtra("flag");
        if(flag.equalsIgnoreCase("failure")){
            ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            //statusDescTxt.setText("UnFortunatly Paymet was rejected.");
            backBtn.setBackgroundResource(R.drawable.button_backgroundtransaction_fail);

            BankResponse.showStatusMessage(manager,RESPONSE_CODE,statusDescTxt);
            PAXScreen.showFailure(manager);
            appl_name.setText(applName);
            a_id.setText(aId);
            rref_num.setText(prefNum);
            mid.setText(MID);
            txn_id.setText(TRANSACTION_ID);
            tid.setText(TID);
            txnid.setText(TXN_ID);
            invoice.setText(INVOICE);
            card_id.setText(CARD_TYPE);
            appr_code.setText(APPR_CODE);
            card_no.setText(CARD_NUMBER);
            card_amount.setText("N/A");

            String transactionType = "";
            if(MATMSDKConstant.transactionType.equalsIgnoreCase("0")){
                transactionType = "BalanceEnquiry Failled!! ";
            }else{
                transactionType = "CashWithdraw Failled!! ";
            }

            String str ="";
            str = statusDescTxt.getText().toString();

            String responseData = generateJsonData(transactionType,str,prefNum,CARD_NUMBER,AMOUNT,TID);
            MATMSDKConstant.responseData =responseData;
        }


       else {
            //Show Success
            PAXScreen.showSuccess(manager);
            appl_name.setText(applName);
            a_id.setText(aId);
            txn_id.setText(TRANSACTION_ID);
            rref_num.setText(prefNum);
            mid.setText(MID);
            tid.setText(TID);
            txnid.setText(TXN_ID);
            invoice.setText(INVOICE);
            card_id.setText(CARD_TYPE);
            appr_code.setText(APPR_CODE);
            card_no.setText(CARD_NUMBER);
            card_amount.setText(AMOUNT);

            String transactionType = "";
            if (MATMSDKConstant.transactionType.equalsIgnoreCase("0")) {
                transactionType = "BalanceEnquiry Successful!! ";
            } else {
                transactionType = "CashWithdraw Successful!! ";
            }

            String str ="";
            str = statusDescTxt.getText().toString();

            String responseData = generateJsonData(transactionType,str, prefNum, CARD_NUMBER, AMOUNT, TID);
            MATMSDKConstant.responseData = responseData;


        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public String replaceWithZero(String s) {
        //int length = s.length();
        //Check whether or not the string contains at least four characters; if not, this method is useless
        float amount = Integer.valueOf(s)/100F;
        DecimalFormat formatter = new DecimalFormat("##,##,##,##0.00");
        return formatter.format(Double.parseDouble(String.valueOf(amount)));
        // return String.valueOf(amount);
    }
    public String generateJsonData(String status,String statusDesc, String rrn,String cardno,String bal,String terminalId){
        String jdata = "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("TransactionStatus",status);
            obj.put("StatusDescription",statusDesc);
            obj.put("RRN",rrn);
            obj.put("CardNumber",cardno);
            obj.put("Balance",bal);
            obj.put("TerminalID",terminalId);
            jdata = obj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jdata;
    }


}
